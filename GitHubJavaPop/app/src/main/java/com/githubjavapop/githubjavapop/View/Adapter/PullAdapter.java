package com.githubjavapop.githubjavapop.View.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.githubjavapop.githubjavapop.Model.Pull;
import com.githubjavapop.githubjavapop.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Hyago on 21/01/2018.
 */

public class PullAdapter extends RecyclerView.Adapter<PullAdapter.ViewHolderPull>{


    private List<Pull> pulls;
    private Context context;

    public PullAdapter(List<Pull> pullList,Context context){
        this.pulls = pullList;
        this.context = context;
    }

    @Override
    public ViewHolderPull onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderPull(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_line_pull,parent,false),pulls,context);
    }

    @Override
    public void onBindViewHolder(ViewHolderPull holder, int position) {
        holder.textViewPullTitle.setText(this.pulls.get(position).getTitle());
        String description = this.pulls.get(position).getDescription();
        if (description != null){
            if (description != ""){
                if (description.length() > 100){
                    description = description.substring(0,100)+"...";
                }
            }
        }
        holder.textViewPullDescription.setText(description);
        holder.textViewUserName.setText(this.pulls.get(position).getUser().getLogin());
        Picasso.with(this.context).load(this.pulls.get(position).getUser().getAvatar_url())
                .resize(35,35)
                .centerCrop().into(holder.imageViewPullUserAvatar);
        try {
            String data = new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy", Locale.US).parse(String.valueOf(this.pulls.get(position).getDate())));
            holder.textViewPullDate.setText(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.pulls.size();
    }

    public class ViewHolderPull extends RecyclerView.ViewHolder implements OnClickListener{

        private TextView textViewPullTitle;
        private TextView textViewPullDescription;
        private TextView textViewUserName;
        private ImageView imageViewPullUserAvatar;
        private TextView textViewPullDate;
        private List<Pull> pulls;
        private Context context;

        public ViewHolderPull(View itemView, List<Pull> pullList, Context context) {
            super(itemView);
            this.pulls = pullList;
            this.context = context;
            textViewPullTitle = itemView.findViewById(R.id.textViewPullTitle);
            textViewPullDescription = itemView.findViewById(R.id.textViewPullDescription);
            textViewUserName = itemView.findViewById(R.id.textViewPullUsername);
            imageViewPullUserAvatar = itemView.findViewById(R.id.imageViewPullUserAvatar);
            textViewPullDate = itemView.findViewById(R.id.textViewPullDate);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(this.pulls.get(getAdapterPosition()).getHtml_url()));
            context.startActivity(intent);
        }
    }
}
