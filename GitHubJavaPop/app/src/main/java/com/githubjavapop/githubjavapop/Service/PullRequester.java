package com.githubjavapop.githubjavapop.Service;

import com.githubjavapop.githubjavapop.Model.Pull;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hyago on 21/01/2018.
 */

public class PullRequester extends CommunicationService{

    public PullRequester(){}

    public List<Pull> getPullList(String repositoryName, String userName){
        List<Pull> pulls = new ArrayList<>();
        try{
            String response = this.request(this.URL_BASE_PULL_REQUEST(repositoryName,userName));
            Gson gson = new Gson();
            JsonArray jsonArray = gson.fromJson(response,JsonArray.class);
            for (int i =0;i<jsonArray.size();i++){
                Pull pull = gson.fromJson(jsonArray.get(i).getAsJsonObject(),Pull.class);
                pulls.add(pull);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pulls;
    }
}
