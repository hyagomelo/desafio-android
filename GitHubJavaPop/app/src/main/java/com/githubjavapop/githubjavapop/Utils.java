package com.githubjavapop.githubjavapop;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by Hyago on 22/01/2018.
 */

public class Utils {

    public Utils(){}

    public boolean checkInternetAcess(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getActiveNetworkInfo() != null){
            if (connectivityManager.getActiveNetworkInfo().isConnected()){
                return true;
            }
        }
        return false;
    }
}
