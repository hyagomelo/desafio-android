package com.githubjavapop.githubjavapop.View.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.githubjavapop.githubjavapop.Model.Repository;
import com.githubjavapop.githubjavapop.View.PullActivity;
import com.githubjavapop.githubjavapop.R;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Hyago on 19/01/2018.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolderRepository> {

    private List<Repository> repositories;
    private Context context;

    public RepositoryAdapter(List<Repository> repositories, Context context) {
        this.repositories = repositories;
        this.context = context;
    }

    @Override
    public ViewHolderRepository onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderRepository(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.layout_line_repository, parent, false), this.context, this.repositories);
    }


    @Override
    public void onBindViewHolder(final ViewHolderRepository holder, final int position) {
        holder.textViewRepositoryName.setText(repositories.get(position).getName());
        if (repositories.get(position).getDescription() != null) {
            String description = repositories.get(position).getDescription();
            if (description.length() > 100) {
                description = description.substring(0, 97) + "...";
            }
            holder.textViewRepositoryDescription.setText(description);
        }
        holder.textViewForksCount.setText(String.valueOf(repositories.get(position).getForks_count()));
        holder.textViewStarsCount.setText(String.valueOf(repositories.get(position).getStargazers_count()));
        holder.textViewUsername.setText(String.valueOf(repositories.get(position).getUser().getLogin()));
        if (this.repositories.get(position).getUser().getAvatar_url() != ""){
            Picasso.with(context).load(repositories.get(position).getUser().getAvatar_url())
                    .resize(200, 200)
                    .centerCrop().into(holder.imageViewAvatar);
        }
    }

    public void addListMoreItems(List<Repository> repositories) {
        this.repositories.addAll(repositories);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.repositories.size();
    }

    public class ViewHolderRepository extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView textViewRepositoryName;
        private TextView textViewRepositoryDescription;
        private TextView textViewForksCount;
        private TextView textViewStarsCount;
        private TextView textViewUsername;
        private ImageView imageViewAvatar;
        private List<Repository> repositories;
        private Context context;

        private ViewHolderRepository(View itemView, Context context, List<Repository> repositories) {
            super(itemView);
            this.context = context;
            this.repositories = repositories;

            textViewRepositoryName = itemView.findViewById(R.id.textViewRepositoryName);
            textViewRepositoryDescription = itemView.findViewById(R.id.textViewRepositoryDescription);
            textViewForksCount = itemView.findViewById(R.id.textViewForksCount);
            textViewStarsCount = itemView.findViewById(R.id.textViewStarsCount);
            textViewUsername = itemView.findViewById(R.id.textViewUserName);
            imageViewAvatar = itemView.findViewById(R.id.imageViewAvatar);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(this.context, PullActivity.class);
            intent.putExtra("userName", this.repositories.get(getAdapterPosition()).getUser().getLogin());
            intent.putExtra("repositoryName", this.repositories.get(getAdapterPosition()).getName());
            this.context.startActivity(intent);
        }
    }
}
