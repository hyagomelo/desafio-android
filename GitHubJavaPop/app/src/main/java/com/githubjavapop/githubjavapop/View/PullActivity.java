package com.githubjavapop.githubjavapop.View;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.githubjavapop.githubjavapop.Model.Pull;
import com.githubjavapop.githubjavapop.R;
import com.githubjavapop.githubjavapop.Service.PullRequester;
import com.githubjavapop.githubjavapop.Utils;
import com.githubjavapop.githubjavapop.View.Adapter.PullAdapter;

import java.util.List;

public class PullActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        Bundle bundle = getIntent().getExtras();

        String userName = null;
        String repositoryName = null;

        if (bundle != null){
            if (bundle.getString("userName") != null){
                if (bundle.getString("userName") != ""){
                    userName = bundle.getString("userName");
                }
            }
            if (bundle.getString("repositoryName") != null){
                if (bundle.getString("repositoryName") != ""){
                    repositoryName = bundle.getString("repositoryName");
                }
            }
        }

        PullRequester pullRequester = new PullRequester();
        Utils utils = new Utils();
        if (!utils.checkInternetAcess(getApplicationContext())) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.linearLayoutMainActivity), "Você não está conectado à internet.", Snackbar.LENGTH_LONG);
            snackbar.show();
        }else{
            List<Pull> pulls = pullRequester.getPullList(repositoryName,userName);
            if (pulls.size() > 0){
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewPulls);
                recyclerView.setHasFixedSize(true);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL));

                PullAdapter pullAdapter = new PullAdapter(pulls,getApplicationContext());
                recyclerView.setAdapter(pullAdapter);
            }else{
                Snackbar snackbar = Snackbar.make(findViewById(R.id.linearLayoutPullActivity), "Nenhum pull request encontrado.", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
