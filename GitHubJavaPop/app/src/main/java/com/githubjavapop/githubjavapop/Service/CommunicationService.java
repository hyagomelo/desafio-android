package com.githubjavapop.githubjavapop.Service;

import android.annotation.SuppressLint;
import android.os.AsyncTask;


import java.util.concurrent.ExecutionException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Hyago on 20/01/2018.
 */

public class CommunicationService{

    public CommunicationService(){}

    public final String URL_BASE_REPOSITORY(int page){
        return "https://api.github.com/search/repositories?q=language:Java&sort=stars&page="+page;
    }
    public final String URL_BASE_PULL_REQUEST(String repositoryName, String userName){
       return  "https://api.github.com/repos/"+userName+"/"+repositoryName+"/pulls";
    }

    public String request(final String url) throws ExecutionException, InterruptedException {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    return response.body().string();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }
        };
        return task.execute().get();
    }
}
