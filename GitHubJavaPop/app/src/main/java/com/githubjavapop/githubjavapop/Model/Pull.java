package com.githubjavapop.githubjavapop.Model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Hyago on 21/01/2018.
 */

public class Pull {

    private String title;
    @SerializedName("body")
    private String description;
    private String html_url;
    @SerializedName("created_at")
    private Date date;
    private User user;

    public Pull(){
        this.user = new User();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
