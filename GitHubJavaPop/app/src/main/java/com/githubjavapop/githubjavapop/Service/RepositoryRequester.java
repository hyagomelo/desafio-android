package com.githubjavapop.githubjavapop.Service;

import com.githubjavapop.githubjavapop.Model.Repository;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hyago on 20/01/2018.
 */

public class RepositoryRequester extends CommunicationService{

    public List<Repository> getRepositoryList(int page){
        List<Repository> repositories = new ArrayList<>();
        try{
            String response = this.request(this.URL_BASE_REPOSITORY(page));
            Gson gson = new Gson();
            JsonObject jsonObject = gson.fromJson(response,JsonObject.class);
            JsonArray jsonArray = jsonObject.getAsJsonArray("items");
            if (jsonArray.size() > 0){
                if (jsonArray != null){
                    for (int i =0;i<jsonArray.size();i++){
                        Repository repository = gson.fromJson(jsonArray.get(i).getAsJsonObject(),Repository.class);
                        repositories.add(repository);
                    }
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return repositories;
    }
}
