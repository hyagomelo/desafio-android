package com.githubjavapop.githubjavapop.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hyago on 20/01/2018.
 */

public class Repository {

    private String name;
    private String description;
    @SerializedName("owner")
    private User user;
    private int stargazers_count;
    private int forks_count;

    public Repository(String name, String description,User owner,int stargazers_count, int forks_count){
        this.name = name;
        this.description = description;
        this.user = owner;
        this.stargazers_count = stargazers_count;
        this.forks_count = forks_count;
    }

    public Repository(){
        this.user = new User();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(int stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public int getForks_count() {
        return forks_count;
    }

    public void setForks_count(int forks_count) {
        this.forks_count = forks_count;
    }
}
