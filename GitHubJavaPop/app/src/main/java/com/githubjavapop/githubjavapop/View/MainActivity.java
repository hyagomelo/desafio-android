package com.githubjavapop.githubjavapop.View;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.githubjavapop.githubjavapop.Model.Repository;
import com.githubjavapop.githubjavapop.R;
import com.githubjavapop.githubjavapop.Service.RepositoryRequester;
import com.githubjavapop.githubjavapop.Utils;
import com.githubjavapop.githubjavapop.View.Adapter.RepositoryAdapter;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RepositoryRequester repositoryRequester;
    private int page = 1;
    private RepositoryAdapter repositoryAdapter;
    private boolean isLoading = false;
    private int totalItems = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        repositoryRequester = new RepositoryRequester();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewRepositories);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                int pos = layoutManager.findLastCompletelyVisibleItemPosition();
                if ((pos + 1) == totalItems && !isLoading) {
                    isLoading = true;
                    loadData();
                }
            }
        });
        loadData();
    }

    public void loadData() {
        Utils utils = new Utils();
        if (!utils.checkInternetAcess(getApplicationContext())) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.linearLayoutMainActivity), "Você não está conectado à Internet.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else {
            final List<Repository> repositoryList = repositoryRequester.getRepositoryList(page);
            if (repositoryList.size() > 0) {
                if (page == 1) {
                    repositoryAdapter = new RepositoryAdapter(repositoryList, getApplicationContext());
                    recyclerView.setAdapter(repositoryAdapter);
                } else {
                    repositoryAdapter.addListMoreItems(repositoryList);
                }
                totalItems += repositoryList.size();
                page++;
                isLoading = false;
            } else {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.linearLayoutMainActivity), "Todos os repositórios foram carregados.", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }
}
